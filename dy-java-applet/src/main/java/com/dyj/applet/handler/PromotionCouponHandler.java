package com.dyj.applet.handler;

import com.dyj.applet.domain.*;
import com.dyj.applet.domain.query.*;
import com.dyj.applet.domain.vo.*;
import com.dyj.common.config.AgentConfiguration;
import com.dyj.common.domain.DySimpleResult;

/**
 * 小程序券
 */
public class PromotionCouponHandler extends AbstractAppletHandler {
    public PromotionCouponHandler(AgentConfiguration agentConfiguration) {
        super(agentConfiguration);
    }

    /**
     * 查询用户可用券信息
     * @param body 查询用户可用券信息请求值
     * @return
     */
    public DySimpleResult<QueryCouponReceiveInfoVo> queryCouponReceiveInfo(QueryCouponReceiveInfoQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().queryCouponReceiveInfo(body);
    }

    /**
     * 用户撤销核销券
     * @param body 用户撤销核销券请求值
     * @return
     */
    public DySimpleResult<ConsumeCouponIdListVo> batchRollbackConsumeCoupon(BatchRollbackConsumeCouponQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().batchRollbackConsumeCoupon(body);
    }


    /**
     * 复访营销活动实时圈选用户
     * @param body 复访营销活动实时圈选用户请求值
     * @return
     */
    public DySimpleResult<?> bindUserToSidebarActivity(BindUserToSidebarActivityQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().bindUserToSidebarActivity(body);
    }


    /**
     * 用户核销券
     * @param body 用户核销券请求值
     * @return
     */
    public DySimpleResult<ConsumeCouponIdListVo> batchConsumeCoupon(BatchConsumeCouponQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().batchConsumeCoupon(body);
    }

    /**
     * 查询主播发券配置信息
     * @param body 查询主播发券配置信息请求值
     * @return
     */
    public DySimpleResult<TalentCouponLimit> queryTalentCouponLimit(QueryTalentCouponLimitQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().queryTalentCouponLimit(body);
    }

    /**
     * 修改主播发券权限状态
     * @param body 修改主播发券权限状态请求值
     * @return
     */
    public DySimpleResult<?> updateTalentCouponStatus(UpdateTalentCouponStatusQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().updateTalentCouponStatus(body);
    }

    /**
     * 更新主播发券库存上限
     * @param body 更新主播发券库存上限请求值
     * @return
     */
    public DySimpleResult<?> updateTalentCouponStock(UpdateTalentCouponStockQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().updateTalentCouponStock(body);
    }

    /**
     * 主播发券权限配置
     * @param body 主播发券权限配置请求值
     * @return
     */
    public DySimpleResult<?> setTalentCouponApi(SetTalentCouponApiQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().setTalentCouponApi(body);
    }

    /**
     * 创建营销活动
     * @param body 创建营销活动请求值
     * @return
     */
    public DySimpleResult<CreatePromotionActivityVo> createPromotionActivityV2(PromotionActivityQuery<CreatePromotionActivityV2> body){
        baseQuery(body);
        return getPromotionCouponClient().createPromotionActivityV2(body);
    }


    /**
     * 修改营销活动
     * @param body 修改营销活动请求值
     * @return
     */
    public DySimpleResult<?> modifyPromotionActivityV2(PromotionActivityQuery<ModifyPromotionActivity> body){
        baseQuery(body);
        return getPromotionCouponClient().modifyPromotionActivityV2(body);
    }

    /**
     * 查询营销活动
     * @param body 查询营销活动请求值
     * @return
     */
    public QueryPromotionActivityV2Vo queryPromotionActivityV2(QueryPromotionActivityV2Query body){
        baseQuery(body);
        return getPromotionCouponClient().queryPromotionActivityV2(body);
    }


    /**
     * 修改营销活动状态
     * @param body 修改营销活动状态请求值
     * @return
     */
    public DySimpleResult<?> updatePromotionActivityStatusV2(UpdatePromotionActivityStatusV2Query body) {
        baseQuery(body);
        return getPromotionCouponClient().updatePromotionActivityStatusV2(body);
    }

    /**
     * 创建券模板
     * @param body 创建券模板请求值
     * @return
     */
    public DySimpleResult<CreateCouponMetaVO> createCouponMetaV2(CouponMetaQuery<CreateCouponMeta> body){
        baseQuery(body);
        return getPromotionCouponClient().createCouponMetaV2(body);
    }


    /**
     * 修改券模板
     * @param body 修改券模板请求值
     * @return
     */
    public DySimpleResult<?> modifyCouponMetaV2(CouponMetaQuery<ModifyCouponMeta> body){
        baseQuery(body);
        return getPromotionCouponClient().modifyCouponMetaV2(body);
    }

    /**
     * 查询券模板
     * @param body 查询券模板请求值
     * @return
     */
    public QueryCouponMetaVo queryCouponMetaV2(QueryCouponMetaQuery body){
        baseQuery(body);
        return getPromotionCouponClient().queryCouponMetaV2(body);
    }

    /**
     * 查询授权用户发放的活动信息
     * @param body 查询授权用户发放的活动信息请求值
     * @return
     */
    public DySimpleResult<QueryActivityMetaDetailListVo> queryActivityMetaDetailList(QueryActivityMetaDetailListQuery body){
        userInfoQuery(body);
        return getPromotionCouponClient().queryActivityMetaDetailList(body);
    }

    /**
     * 删除券模板
     * @param body 删除券模板请求值
     * @return
     */
    public DySimpleResult<?> cancelCouponMetaApi(CancelCouponMetaApiQuery body){
        baseQuery(body);
        return getPromotionCouponClient().cancelCouponMetaApi(body);
    }

    /**
     * 修改券模板库存
     * @param body 修改券模板库存请求值
     * @return
     */
    public DySimpleResult<?> updateCouponMetaStockApi(UpdateCouponMetaStockQuery body){
        baseQuery(body);
        return getPromotionCouponClient().updateCouponMetaStockApi(body);
    }


    /**
     * 修改券模板状态
     * @param body 修改券模板状态请求值
     * @return
     */
    public DySimpleResult<?> updateCouponMetaStatus(UpdateCouponMetaStatusQuery body){
        baseQuery(body);
        return getPromotionCouponClient().updateCouponMetaStatus(body);
    }

    /**
     * 查询券模板发放统计数据
     * @param body 查询券模板发放统计数据请求值
     * @return
     */
    public DySimpleResult<QueryCouponMetaStatisticsVo> queryCouponMetaStatistics(QueryCouponMetaStatisticsQuery body){
        baseQuery(body);
        return getPromotionCouponClient().queryCouponMetaStatistics(body);
    }

    /**
     * 查询对账单
     * @param body 查询对账单请求值
     * @return
     */
    public DySimpleResult<GetBillDownloadUrl> getBillDownloadUrl(GetBillDownloadUrlQuery body){
        baseQuery(body);
        return getPromotionCouponClient().getBillDownloadUrl(body);
    }

    /**
     * 创建开发者接口发券活动
     * @param body 创建开发者接口发券活动请求值
     * @return
     */
    public DySimpleResult<CreateDeveloperActivityActivityId>  createDeveloperActivity(CreateDeveloperActivityQuery body){
        baseQuery(body);
        return getPromotionCouponClient().createDeveloperActivity(body);
    }

    /**
     * 开发者接口发券
     * @param body 开发者接口发券请求值
     * @return
     */
    public DySimpleResult<SendCouponToDesignatedUserVo> sendCouponToDesignatedUser(SendCouponToDesignatedUserQuery body){
        baseQuery(body);
        return getPromotionCouponClient().sendCouponToDesignatedUser(body);
    }

    /**
     * 删除开发者接口发券活动
     * @param body 删除开发者接口发券活动请求值
     * @return
     */
    public DySimpleResult<?> deleteDeveloperActivity(DeleteDeveloperActivityQuery body){
        baseQuery(body);
        return getPromotionCouponClient().deleteDeveloperActivity(body);
    }

}
