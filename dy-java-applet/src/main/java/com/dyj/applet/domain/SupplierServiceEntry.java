package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-28 15:21
 **/
public class SupplierServiceEntry {

    /**
     * 抖音小程序入口参数
     */
    private EntryMiniApp entry_mini_app;

    /**
     * 入口类型(1:H5，2:抖音小程序)
     */
    private Integer entry_type;

    public static SupplierServiceEntryBuilder builder() {
        return new SupplierServiceEntryBuilder();
    }

    public static class SupplierServiceEntryBuilder {
        private EntryMiniApp entryMiniApp;
        private Integer entryType;

        public SupplierServiceEntryBuilder entryMiniApp(EntryMiniApp entryMiniApp) {
            this.entryMiniApp = entryMiniApp;
            return this;
        }
        public SupplierServiceEntryBuilder entryType(Integer entryType) {
            this.entryType = entryType;
            return this;
        }
        public SupplierServiceEntry build() {
            SupplierServiceEntry supplierServiceEntry = new SupplierServiceEntry();
            supplierServiceEntry.setEntry_mini_app(entryMiniApp);
            supplierServiceEntry.setEntry_type(entryType);
            return supplierServiceEntry;
        }
    }

    public EntryMiniApp getEntry_mini_app() {
        return entry_mini_app;
    }

    public void setEntry_mini_app(EntryMiniApp entry_mini_app) {
        this.entry_mini_app = entry_mini_app;
    }

    public Integer getEntry_type() {
        return entry_type;
    }

    public void setEntry_type(Integer entry_type) {
        this.entry_type = entry_type;
    }
}
