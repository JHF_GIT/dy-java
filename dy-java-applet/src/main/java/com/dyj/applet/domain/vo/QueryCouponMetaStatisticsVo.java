package com.dyj.applet.domain.vo;

import com.dyj.applet.domain.QueryCouponMetaStatisticsActivityStats;
import com.dyj.applet.domain.QueryCouponMetaStatisticsTalentStats;

import java.util.List;

/**
 * 查询券模板发放统计数据返回值
 */
public class QueryCouponMetaStatisticsVo {


    /**
     *  选填
     */
    private List<QueryCouponMetaStatisticsActivityStats> activity_stats;
    /**
     *  选填
     */
    private List<QueryCouponMetaStatisticsTalentStats> talent_stats;

    public List<QueryCouponMetaStatisticsActivityStats> getActivity_stats() {
        return activity_stats;
    }

    public QueryCouponMetaStatisticsVo setActivity_stats(List<QueryCouponMetaStatisticsActivityStats> activity_stats) {
        this.activity_stats = activity_stats;
        return this;
    }

    public List<QueryCouponMetaStatisticsTalentStats> getTalent_stats() {
        return talent_stats;
    }

    public QueryCouponMetaStatisticsVo setTalent_stats(List<QueryCouponMetaStatisticsTalentStats> talent_stats) {
        this.talent_stats = talent_stats;
        return this;
    }
}
