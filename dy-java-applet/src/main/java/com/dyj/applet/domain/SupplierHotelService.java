package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-28 14:01
 **/
public class SupplierHotelService {

    /**
     * 设施/服务code
     * 1 - 空调;
     * 2 - 电视;
     * 3 - 阳台;
     * 4 - 窗户;
     * 5 - 独立卫浴;
     * 6 - 浴缸;
     * 7 - 吹风机;
     * 8 - 衣架;
     * 9 - 热水;
     * 10 - 洗衣机;
     * 11 - 基本厨具;
     * 12 - 冰箱;
     * 13 - 免费Wifi;
     * 14 - 电热水壶;
     * 15 - 暖气;
     * 16 - 智能马桶;
     * 17 - 微波炉;
     * 18 - 门禁系统;
     * 19 - 智能门锁;
     * 20 - 私家花园;
     * 21 - 私家泳池;
     * 22 - 观景露台;
     * 23 - 免费停车;
     * 24 - 行李寄存;
     */
    private String code;
    /**
     * 设施/服务名称, code与name不匹配时，code生效。
     * 自定义项目不超过5个汉字
     */
    private String name;

    public static SupplierHotelServiceBuilder builder() {
        return new SupplierHotelServiceBuilder();
    }

    public static class SupplierHotelServiceBuilder {
        private String code;
        private String name;
        public SupplierHotelServiceBuilder code(String code) {
            this.code = code;
            return this;
        }
        public SupplierHotelServiceBuilder name(String name) {
            this.name = name;
            return this;
        }
        public SupplierHotelService build() {
            SupplierHotelService supplierHotelService = new SupplierHotelService();
            supplierHotelService.setCode(code);
            supplierHotelService.setName(name);
            return supplierHotelService;
        }
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
