package com.dyj.applet.domain;

/**
 * @author danmo
 * @date 2024-04-29 14:55
 **/
public class SpuEntryInfo {

    /**
     * 小程序入口参数
     */
    private EntryMiniApp entry_miniApp;

    /**
     * 入口类型(1:H5，2:抖音小程序，3:抖音链接)
     */
    private Integer entry_type;

    /**
     * 入口链接
     */
    private String entry_url;

    public static SpuEntryInfoBuilder builder() {
        return new SpuEntryInfoBuilder();
    }

    public static class SpuEntryInfoBuilder {
        private EntryMiniApp entryMiniApp;

        private Integer entryType;

        private String entryUrl;

        public SpuEntryInfoBuilder entryMiniApp(EntryMiniApp entryMiniApp) {
            this.entryMiniApp = entryMiniApp;
            return this;
        }

        public SpuEntryInfoBuilder entryType(Integer entryType) {
            this.entryType = entryType;
            return this;
        }

        public SpuEntryInfoBuilder entryUrl(String entryUrl) {
            this.entryUrl = entryUrl;
            return this;
        }

        public SpuEntryInfo build() {
            SpuEntryInfo spuEntryInfo = new SpuEntryInfo();
            spuEntryInfo.setEntry_miniApp(entryMiniApp);
            spuEntryInfo.setEntry_type(entryType);
            spuEntryInfo.setEntry_url(entryUrl);
            return spuEntryInfo;
        }
    }

    public EntryMiniApp getEntry_miniApp() {
        return entry_miniApp;
    }

    public void setEntry_miniApp(EntryMiniApp entry_miniApp) {
        this.entry_miniApp = entry_miniApp;
    }

    public Integer getEntry_type() {
        return entry_type;
    }

    public void setEntry_type(Integer entry_type) {
        this.entry_type = entry_type;
    }

    public String getEntry_url() {
        return entry_url;
    }

    public void setEntry_url(String entry_url) {
        this.entry_url = entry_url;
    }
}
