package com.dyj.applet.domain;

/**
 * 查询券模板发放统计数据 活动统计
 */
public class QueryCouponMetaStatisticsActivityStats {

    /**
     * <p>接口发券活动id</p>
     */
    private String activity_id;
    /**
     * <p>通过接口发券获得对应券模板的核销数量</p>
     */
    private Long consumed_num;
    /**
     * <p>接口发券对应券模板的领取数</p>
     */
    private Long received_num;

    public String getActivity_id() {
        return activity_id;
    }

    public QueryCouponMetaStatisticsActivityStats setActivity_id(String activity_id) {
        this.activity_id = activity_id;
        return this;
    }

    public Long getConsumed_num() {
        return consumed_num;
    }

    public QueryCouponMetaStatisticsActivityStats setConsumed_num(Long consumed_num) {
        this.consumed_num = consumed_num;
        return this;
    }

    public Long getReceived_num() {
        return received_num;
    }

    public QueryCouponMetaStatisticsActivityStats setReceived_num(Long received_num) {
        this.received_num = received_num;
        return this;
    }
}
