package com.dyj.applet.domain;

import com.alibaba.fastjson.JSONObject;

import java.util.List;

public class OrientedPlanTalent {

    /**
     * 计划ID
     */
    private Long plan_id;

    /**
     * 计划名称
     */
    private String plan_name;
    /**
     * 定向佣金状态。1：在线/进行中，2：已完成，3：已取消
     */
    private Integer status;
    /**
     * 计划类型：1:短视频场景2:直播间场景
     */
    private Integer content_type;
    /**
     * 佣金有效期，直播间场景下的计划此字段为0
     */
    private Long commission_duration;

    /**
     * 计划开始时间，直播间场景下的计划此字段为0
     */
    private Long start_time;

    /**
     * 计划结束时间，直播间场景下的计划此字段为0
     */
    private Long end_time;

    /**
     * 达人抖音号列表
     */
    private List<String> douyin_id_list;

    /**
     * 达人抖音号   达人履约状态：1:进行中2:已完成3:已取消
     */
    private JSONObject talent_status_map;

    private List<LivePlanProduct> product_list;
    /**
     * 计划创建时间
     */
    private String create_time;

    /**
     * 创建时间戳
     */
    private Long create_time_unix;

    /**
     * 更新时间
     */
    private String update_time;

    /**
     * 更新时间戳
     */
    private Long update_time_unix;

    public Long getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(Long plan_id) {
        this.plan_id = plan_id;
    }

    public String getPlan_name() {
        return plan_name;
    }

    public void setPlan_name(String plan_name) {
        this.plan_name = plan_name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getContent_type() {
        return content_type;
    }

    public void setContent_type(Integer content_type) {
        this.content_type = content_type;
    }

    public Long getCommission_duration() {
        return commission_duration;
    }

    public void setCommission_duration(Long commission_duration) {
        this.commission_duration = commission_duration;
    }

    public Long getStart_time() {
        return start_time;
    }

    public void setStart_time(Long start_time) {
        this.start_time = start_time;
    }

    public Long getEnd_time() {
        return end_time;
    }

    public void setEnd_time(Long end_time) {
        this.end_time = end_time;
    }

    public List<String> getDouyin_id_list() {
        return douyin_id_list;
    }

    public void setDouyin_id_list(List<String> douyin_id_list) {
        this.douyin_id_list = douyin_id_list;
    }

    public JSONObject getTalent_status_map() {
        return talent_status_map;
    }

    public void setTalent_status_map(JSONObject talent_status_map) {
        this.talent_status_map = talent_status_map;
    }

    public List<LivePlanProduct> getProduct_list() {
        return product_list;
    }

    public void setProduct_list(List<LivePlanProduct> product_list) {
        this.product_list = product_list;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public Long getCreate_time_unix() {
        return create_time_unix;
    }

    public void setCreate_time_unix(Long create_time_unix) {
        this.create_time_unix = create_time_unix;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public Long getUpdate_time_unix() {
        return update_time_unix;
    }

    public void setUpdate_time_unix(Long update_time_unix) {
        this.update_time_unix = update_time_unix;
    }
}
