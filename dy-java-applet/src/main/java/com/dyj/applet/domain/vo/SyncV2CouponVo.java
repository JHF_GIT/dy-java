package com.dyj.applet.domain.vo;

import com.dyj.common.domain.vo.BaseVo;

/**
 * @author danmo
 * @date 2024-05-06 16:49
 **/
public class SyncV2CouponVo extends BaseVo {

    /**
     * 	抖音优惠券id
     */
    private String coupon_id;
    /**
     * 是否同步成功 true成功 false失败
     */
    private Boolean is_suc;

    public String getCoupon_id() {
        return coupon_id;
    }

    public void setCoupon_id(String coupon_id) {
        this.coupon_id = coupon_id;
    }

    public Boolean getIs_suc() {
        return is_suc;
    }

    public void setIs_suc(Boolean is_suc) {
        this.is_suc = is_suc;
    }
}
