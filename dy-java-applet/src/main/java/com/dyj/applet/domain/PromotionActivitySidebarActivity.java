package com.dyj.applet.domain;

/**
 * 创建营销活动-复访营销活动
 */
public class PromotionActivitySidebarActivity {

    /**
     * <p>复访营销活动的引流方式</p>
     */
    private Integer action_trigger;
    /**
     * <p>当action_trigger=1或2时，可以通过该字段声明用户需要完成几次该动作以看到复访营销弹窗 如action_trigger=1，complete_times=2时，代表用户需要看2次激励广告才能看到复访营销弹窗 选填
     */
    private Integer complete_times;
    /**
     * <p>用户通过复访链路访问侧边栏时高价值区展示的内容（少于11个字）</p>
     */
    private String high_value_content;
    /**
     * <p>用户通过复访链路访问侧边栏时高价值区跳转按钮展示的内容，只允许选择以下内容并传入 去使用 去领取 免费看 领优惠
     */
    private String jump_text;
    /**
     * <p>通过圈选人群 OpenAPI，将用户绑定到特定福利上，实现定向展示福利内容。 选填
     */
    private Boolean need_bind;
    /**
     * <p>用户通过复访链路访问侧边栏时最近使用区小程序右上角气泡展示的内容，只允许选择以下内容并传入 有福利 领福利 免费看 有优惠
     */
    private String recent_bubble_text;
    /**
     * <p>营销活动短标题（少于5个字）</p><ul><li>用于用户端弹窗和复访时展示，可以简单强调营销内容</li><li>需审核</li></ul>
     */
    private String short_title;
    /**
     * <p>营销活动生效时间（即可领取开始时间），单位秒</p><ul><li>营销活动的声明周期必须为关联券模板生命周期的子集</li></ul>
     */
    private Long valid_begin_time;
    /**
     * <p>营销活动过期时间（即可领取结束时间），单位秒</p><ul><li>营销活动的声明周期必须为关联券模板生命周期的子集</li></ul>
     */
    private Long valid_end_time;


    public Integer getAction_trigger() {
        return action_trigger;
    }

    public PromotionActivitySidebarActivity setAction_trigger(Integer action_trigger) {
        this.action_trigger = action_trigger;
        return this;
    }

    public Integer getComplete_times() {
        return complete_times;
    }

    public PromotionActivitySidebarActivity setComplete_times(Integer complete_times) {
        this.complete_times = complete_times;
        return this;
    }

    public String getHigh_value_content() {
        return high_value_content;
    }

    public PromotionActivitySidebarActivity setHigh_value_content(String high_value_content) {
        this.high_value_content = high_value_content;
        return this;
    }

    public String getJump_text() {
        return jump_text;
    }

    public PromotionActivitySidebarActivity setJump_text(String jump_text) {
        this.jump_text = jump_text;
        return this;
    }

    public Boolean getNeed_bind() {
        return need_bind;
    }

    public PromotionActivitySidebarActivity setNeed_bind(Boolean need_bind) {
        this.need_bind = need_bind;
        return this;
    }

    public String getRecent_bubble_text() {
        return recent_bubble_text;
    }

    public PromotionActivitySidebarActivity setRecent_bubble_text(String recent_bubble_text) {
        this.recent_bubble_text = recent_bubble_text;
        return this;
    }

    public String getShort_title() {
        return short_title;
    }

    public PromotionActivitySidebarActivity setShort_title(String short_title) {
        this.short_title = short_title;
        return this;
    }

    public Long getValid_begin_time() {
        return valid_begin_time;
    }

    public PromotionActivitySidebarActivity setValid_begin_time(Long valid_begin_time) {
        this.valid_begin_time = valid_begin_time;
        return this;
    }

    public Long getValid_end_time() {
        return valid_end_time;
    }

    public PromotionActivitySidebarActivity setValid_end_time(Long valid_end_time) {
        this.valid_end_time = valid_end_time;
        return this;
    }

    public static CreatePromotionActivitySidebarActivityBuilder builder(){
        return new CreatePromotionActivitySidebarActivityBuilder();
    }


    public static final class CreatePromotionActivitySidebarActivityBuilder {
        private Integer action_trigger;
        private Integer complete_times;
        private String high_value_content;
        private String jump_text;
        private Boolean need_bind;
        private String recent_bubble_text;
        private String short_title;
        private Long valid_begin_time;
        private Long valid_end_time;

        private CreatePromotionActivitySidebarActivityBuilder() {
        }

        public static CreatePromotionActivitySidebarActivityBuilder aCreatePromotionActivitySidebarActivity() {
            return new CreatePromotionActivitySidebarActivityBuilder();
        }

        public CreatePromotionActivitySidebarActivityBuilder actionTrigger(Integer actionTrigger) {
            this.action_trigger = actionTrigger;
            return this;
        }

        public CreatePromotionActivitySidebarActivityBuilder completeTimes(Integer completeTimes) {
            this.complete_times = completeTimes;
            return this;
        }

        public CreatePromotionActivitySidebarActivityBuilder highValueContent(String highValueContent) {
            this.high_value_content = highValueContent;
            return this;
        }

        public CreatePromotionActivitySidebarActivityBuilder jumpText(String jumpText) {
            this.jump_text = jumpText;
            return this;
        }

        public CreatePromotionActivitySidebarActivityBuilder needBind(Boolean needBind) {
            this.need_bind = needBind;
            return this;
        }

        public CreatePromotionActivitySidebarActivityBuilder recentBubbleText(String recentBubbleText) {
            this.recent_bubble_text = recentBubbleText;
            return this;
        }

        public CreatePromotionActivitySidebarActivityBuilder shortTitle(String shortTitle) {
            this.short_title = shortTitle;
            return this;
        }

        public CreatePromotionActivitySidebarActivityBuilder validBeginTime(Long validBeginTime) {
            this.valid_begin_time = validBeginTime;
            return this;
        }

        public CreatePromotionActivitySidebarActivityBuilder validEndTime(Long validEndTime) {
            this.valid_end_time = validEndTime;
            return this;
        }

        public PromotionActivitySidebarActivity build() {
            PromotionActivitySidebarActivity promotionActivitySidebarActivity = new PromotionActivitySidebarActivity();
            promotionActivitySidebarActivity.setAction_trigger(action_trigger);
            promotionActivitySidebarActivity.setComplete_times(complete_times);
            promotionActivitySidebarActivity.setHigh_value_content(high_value_content);
            promotionActivitySidebarActivity.setJump_text(jump_text);
            promotionActivitySidebarActivity.setNeed_bind(need_bind);
            promotionActivitySidebarActivity.setRecent_bubble_text(recent_bubble_text);
            promotionActivitySidebarActivity.setShort_title(short_title);
            promotionActivitySidebarActivity.setValid_begin_time(valid_begin_time);
            promotionActivitySidebarActivity.setValid_end_time(valid_end_time);
            return promotionActivitySidebarActivity;
        }
    }
}
